{
  "classifierPath" : "meta::pure::metamodel::type::Class",
  "content" : {
    "_type" : "class",
    "name" : "CreditEvents",
    "package" : "model::external::cdm",
    "properties" : [ {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "bankruptcy",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. The reference entity has been dissolved or has become insolvent. It also covers events that may be a precursor to insolvency such as instigation of bankruptcy or insolvency proceedings. Sovereign trades are not subject to Bankruptcy as 'technically' a Sovereign cannot become bankrupt. ISDA 2003 Term: Bankruptcy."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "failureToPay",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. This credit event triggers, after the expiration of any applicable grace period, if the reference entity fails to make due payments in an aggregate amount of not less than the payment requirement on one or more obligations (e.g. a missed coupon payment). ISDA 2003 Term: Failure to Pay."
      } ],
      "type" : "model::external::cdm::FailureToPay"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "failureToPayPrincipal",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. Corresponds to the failure by the Reference Entity to pay an expected principal amount or the payment of an actual principal amount that is less than the expected principal amount. ISDA 2003 Term: Failure to Pay Principal."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "failureToPayInterest",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. Corresponds to the failure by the Reference Entity to pay an expected interest amount or the payment of an actual interest amount that is less than the expected interest amount. ISDA 2003 Term: Failure to Pay Interest."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "obligationDefault",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. One or more of the obligations have become capable of being declared due and payable before they would otherwise have been due and payable as a result of, or on the basis of, the occurrence of a default, event of default or other similar condition or event other than failure to pay. ISDA 2003 Term: Obligation Default."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "obligationAcceleration",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. One or more of the obligations have been declared due and payable before they would otherwise have been due and payable as a result of, or on the basis of, the occurrence of a default, event of default or other similar condition or event other than failure to pay (preferred by the market over Obligation Default, because more definitive and encompasses the definition of Obligation Default - this is more favorable to the Seller). Subject to the default requirement amount. ISDA 2003 Term: Obligation Acceleration."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "repudiationMoratorium",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. The reference entity, or a governmental authority, either refuses to recognise or challenges the validity of one or more obligations of the reference entity, or imposes a moratorium thereby postponing payments on one or more of the obligations of the reference entity. Subject to the default requirement amount. ISDA 2003 Term: Repudiation/Moratorium."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "restructuring",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. A restructuring is an event that materially impacts the reference entity's obligations, such as an interest rate reduction, principal reduction, deferral of interest or principal, change in priority ranking, or change in currency or composition of payment. ISDA 2003 Term: Restructuring."
      } ],
      "type" : "model::external::cdm::Restructuring"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "governmentalIntervention",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. A governmental intervention is an event resulting from an action by a governmental authority that materially impacts the reference entity's obligations, such as an interest rate reduction, principal reduction, deferral of interest or principal, change in priority ranking, or change in currency or composition of payment. ISDA 2014 Term: Governmental Intervention."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "distressedRatingsDowngrade",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. Results from the fact that the rating of the reference obligation is down-graded to a distressed rating level. From a usage standpoint, this credit event is typically not applicable in case of RMBS trades."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "maturityExtension",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. Results from the fact that the underlier fails to make principal payments as expected."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "writedown",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. Results from the fact that the underlier writes down its outstanding principal amount."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "impliedWritedown",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A credit event. Results from the fact that losses occur to the underlying instruments that do not result in reductions of the outstanding principal of the reference obligation."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "defaultRequirement",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "In relation to certain credit events, serves as a threshold for Obligation Acceleration, Obligation Default, Repudiation/Moratorium and Restructuring. Market standard is USD 10,000,000 (JPY 1,000,000,000 for all Japanese Yen trades). This is applied on an aggregate or total basis across all Obligations of the Reference Entity. Used to prevent technical/operational errors from triggering credit events. ISDA 2003 Term: Default Requirement."
      } ],
      "type" : "model::external::cdm::Money"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "creditEventNotice",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A specified condition to settlement. An irrevocable written or verbal notice that describes a credit event that has occurred. The notice is sent from the notifying party (either the buyer or the seller) to the counterparty. It provides information relevant to determining that a credit event has occurred. This is typically accompanied by Publicly Available Information. ISDA 2003 Term: Credit Event Notice."
      } ],
      "type" : "model::external::cdm::CreditEventNotice"
    } ],
    "stereotypes" : [ {
      "profile" : "model::external::cdm::metadata",
      "value" : "key"
    } ],
    "taggedValues" : [ {
      "tag" : {
        "profile" : "meta::pure::profiles::doc",
        "value" : "doc"
      },
      "value" : "A class to specify the applicable Credit Events that would trigger a settlement, as specified in the related Confirmation and defined in the ISDA 2014 Credit Definition article IV section 4.1."
    } ]
  }
}