{
  "classifierPath" : "meta::pure::metamodel::type::Class",
  "content" : {
    "_type" : "class",
    "name" : "CreditSupportAgreementElections",
    "package" : "model::external::cdm",
    "properties" : [ {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "regime",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The Regime Table provision , which determines the regulatory regime(s) applicable to each of the parties to the agreement."
      } ],
      "type" : "model::external::cdm::Regime"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "oneWayProvisions",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The determination of whether the One Way Provisions are applicable (true) or not applicable (false)."
      } ],
      "type" : "model::external::cdm::OneWayProvisions"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "generalSimmElections",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The specification of the ISDA SIMM Method for all Covered Transactions with respect to all Regimes."
      } ],
      "type" : "model::external::cdm::GeneralSimmElections"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "identifiedCrossCurrencySwap",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The qualification of whether cross-currency swaps need to be identified in the Confirmation so that the obligations to exchange principal be disregarded for the purpose of determining the Delivery Amount or Return Amount."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "sensitivityMethodologies",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The specification of methodologies to compute sensitivities specific to the agreement."
      } ],
      "type" : "model::external::cdm::SensitivityMethodologies"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "fxHaircutCurrency",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The reference currency for the purpose of specifying the FX Haircut relating to a posting obligation, as being either the Termination Currency or an FX Designated Currency."
      } ],
      "type" : "model::external::cdm::FxHaircutCurrency"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "postingObligations",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The security providers posting obligations."
      } ],
      "type" : "model::external::cdm::PostingObligations"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "substitutedRegime",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The specification of Additional regimes for purposes of determining whether a Regulatory Event has occurred."
      } ],
      "type" : "model::external::cdm::SubstitutedRegime"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "baseAndEligibleCurrency",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The base and eligible currency(ies) for the document as specified by the parties to the agreement."
      } ],
      "type" : "model::external::cdm::BaseAndEligibleCurrency"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "additionalObligations",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The additional obligations that might be specified by the parties to a Credit Support Agreement."
      } ],
      "type" : "String"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "coveredTransactions",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The specification of transactions covered by the terms of the agreement."
      } ],
      "type" : "model::external::cdm::CoveredTransactions"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "creditSupportObligations",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The Credit Support Obligations applicable to the agreement."
      } ],
      "type" : "model::external::cdm::CreditSupportObligations"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "exchangeDate",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The bespoke exchange date terms that might be specified by the parties to the agreement."
      } ],
      "type" : "String"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "calculationAndTiming",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The set of elections for determining Valuation and Timing terms specific to the agreement"
      } ],
      "type" : "model::external::cdm::CalculationAndTiming"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "conditionsPrecedent",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The set of elections that may overwrite the default Condition Precedent provision, and the set of provisions that are deemed Access Condition."
      } ],
      "type" : "model::external::cdm::ConditionsPrecedent"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "substitution",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The conditions under which the Security Provider can substitute posted collateral."
      } ],
      "type" : "model::external::cdm::Substitution"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "disputeResolution",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The election terms under which a party disputes (i) the Calculation Agent’s calculation of a Delivery Amount or a Return Amount, or (ii) the Value of any Transfer of Eligible Credit Support or Posted Credit Support."
      } ],
      "type" : "model::external::cdm::DisputeResolution"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "holdingAndUsingPostedCollateral",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The elections for the holding and using of posted collateral by the respective parties to the Credit Support Annex for Variation Margin."
      } ],
      "type" : "model::external::cdm::HoldingAndUsingPostedCollateral"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "rightsEvents",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The bespoke provisions that might be specified by the parties to the agreement to specify the rights of Security Taker and/or Security Provider when an Early Termination or Access Condition event has occurred.."
      } ],
      "type" : "model::external::cdm::RightsEvents"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "custodyArrangements",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The Custodian and Segregated Account details in respect of each party to the agreement."
      } ],
      "type" : "model::external::cdm::CustodyArrangements"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "distributionAndInterestPayment",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The Distributions and Interest Payment terms specified as part of the agreement."
      } ],
      "type" : "model::external::cdm::DistributionAndInterestPayment"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "creditSupportOffsets",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The specification of whether the standard Credit Support Offset provisions are applicable (true) or not applicable (false)."
      } ],
      "type" : "Boolean"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "additionalRepresentations",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The specification Additional Representations that may be applicable to the agreement."
      } ],
      "type" : "model::external::cdm::AdditionalRepresentations"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "otherEligibleAndPostedSupport",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The Other Eligible Support elections associated with margin agreements."
      } ],
      "type" : "model::external::cdm::OtherEligibleAndPostedSupport"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "demandsAndNotices",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The optional specification of address where the demands, specifications and notices will be communicated to for each of the parties to the agreement."
      } ],
      "type" : "model::external::cdm::ContactElection"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "addressesForTransfer",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The optional specification of address for transfer as specified by the respective parties to the agreement."
      } ],
      "type" : "model::external::cdm::ContactElection"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "otherAgreements",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The bespoke definition of other agreement terms as specified by the parties to the agreement."
      } ],
      "type" : "model::external::cdm::OtherAgreements"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "terminationCurrencyAmendment",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The bespoke provision that might be specified by the parties to the agreement applicable to Termination Currency.  Unless specified the definition of Termination Currency has the meaning specified in the Schedule to the ISDA Master Agreement."
      } ],
      "type" : "model::external::cdm::TerminationCurrencyAmendment"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "minimumTransferAmountAmendment",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The bespoke provision that might be specified by the parties to the agreement applicable to Minimum Transfer Amount.  Unless specified the definition of Minimum Transfer Amount in any Other Regulatory CSA has the meaning specified in such Other Regulatory CSA."
      } ],
      "type" : "model::external::cdm::MinimumTransferAmountAmendment"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "interpretationTerms",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The bespoke provision that might be specified by the parties to the agreement applicable to Interpretations."
      } ],
      "type" : "String"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "processAgent",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The Process Agent that might be appointed by the parties to the agreement."
      } ],
      "type" : "model::external::cdm::ProcessAgent"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "appropriatedCollateralValuation",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The election for the Valuation of Appropriate Collateral."
      } ],
      "type" : "model::external::cdm::AppropriatedCollateralValuation"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "jurisdictionRelatedTerms",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The jurisdiction specific terms relevant to the agreement."
      } ],
      "type" : "model::external::cdm::JurisdictionRelatedTerms"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "additionalAmendments",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Any additional amendments that might be specified by the parties to the agreement."
      } ],
      "type" : "String"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "additionalBespokeTerms",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Any additional terms that might be specified applicable."
      } ],
      "type" : "String"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "trustSchemeAddendum",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The qualification of whether Trust Scheme Addendum is applicable (True) or not applicable (False)."
      } ],
      "type" : "Boolean"
    } ],
    "taggedValues" : [ {
      "tag" : {
        "profile" : "meta::pure::profiles::doc",
        "value" : "doc"
      },
      "value" : "The set of elections which specify a Credit Support Annex or Deed."
    } ]
  }
}